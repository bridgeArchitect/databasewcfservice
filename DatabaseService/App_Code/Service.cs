﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using MySql.Data.MySqlClient;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService
{

    /* constants to connect with MySQL server */
    const string server = "127.0.0.1";
    const string database = "CarDatabase";
    const string user = "root";
    const string password = "London2012";
    const string port = "3306";
    const string sslM = "none";
    /* rows for connection to database */
    string connectionRow = String.Format("server={0}; port={1}; user id={2}; password={3}; database={4}; SslMode={5}; CharSet=cp1251;", server, port, user, password, database, sslM);

    public string GetData(int value)
	{
		return string.Format("You entered: {0}", value);
	}

    public string getAllCars()
    {

        /* answer for query */
        string answer = "";
        /* row for query */
        string query = "select * from car";

        try
        {
            /* make connection and query */
            MySqlConnection connection = new MySqlConnection(connectionRow);
            connection.Open();
            MySqlCommand command = new MySqlCommand(query, connection);

            /* read database */
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                answer += "Id:" + reader[0].ToString() + " Name:" + reader[1].ToString() + " Type:" + reader[2].ToString() + "\r\n";
            }
        }
        catch (Exception exc)
        {
            /* return error */
            return exc.Message + "\r\n";
        }

        /* return answer */
        return answer;

    }

    public string getCarById(int id)
    {

        /* answer for query */
        string answer = "";
        /* row for query */
        string query = string.Format("select * from car where id = {0}", id.ToString());

        try
        {
            /* make connection and query */
            MySqlConnection connection = new MySqlConnection(connectionRow);
            connection.Open();
            MySqlCommand command = new MySqlCommand(query, connection);

            /* read database */
            MySqlDataReader reader = command.ExecuteReader();
            reader.Read();
            answer += "Id:" + reader[0].ToString() + " Name:" + reader[1].ToString() + " Type:" + reader[2].ToString() + "\r\n";
        }
        catch (Exception exc)
        {
            /* return error */
            return exc.Message + "\r\n";
        }

        /* return answer */
        return answer;

    }

    public string getAllCarsWithFactory()
    {

        /* answer for query */
        string answer = "";
        /* row for query */
        string query = "select * from car join factory on car.idFact=factory.id";

        try
        {
            /* make connection and query */
            MySqlConnection connection = new MySqlConnection(connectionRow);
            connection.Open();
            MySqlCommand command = new MySqlCommand(query, connection);

            /* read database */
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                answer += "Id:" + reader[0].ToString() + " Name:" + reader[1].ToString() + " Type:" + reader[2].ToString() + " Id of factory:" + reader[3].ToString() + " Name of factory:" + reader[5].ToString() + " Country:" + reader[6].ToString() + "\r\n";
            }
        }
        catch (Exception exc)
        {
            /* return error */
            return exc.Message + "\r\n";
        }

        /* return answer */
        return answer;

    }

    public string insertNewCar(string name, string type, string factory)
    {

        /* row for query (find id of factory) */
        string queryFind = "select id from factory where name = @name";
        /* id of factory */
        int idFact = 0;

        try
        {
            /* make connection and query */
            MySqlConnection connection = new MySqlConnection(connectionRow);
            connection.Open();
            MySqlCommand command = new MySqlCommand(queryFind, connection);

            /* add all paramaters for request */
            command.Parameters.AddWithValue("@name", factory);

            /* read database (find id of query) */
            MySqlDataReader reader = command.ExecuteReader();
            reader.Read();
            idFact = Convert.ToInt32(reader[0].ToString());

            /* close connection */
            connection.Close();
        }
        catch (Exception exc)
        {
            /* return error */
            return exc.Message + "\r\n";
        }

        /* row for query */
        string queryAdd = "insert into Car (name,type,idFact) values(@name, @type, @idFact)";
        /* answer for query */
        string answer = "";

        using (MySqlConnection connection = new MySqlConnection(connectionRow))
        {
            /* make connection and query */
            connection.Open();
            MySqlCommand commAdd = new MySqlCommand(queryAdd, connection);

            /* add all paramaters for request */
            commAdd.Parameters.Add("@name", MySqlDbType.VarChar);
            commAdd.Parameters["@name"].Value = name;
            commAdd.Parameters.Add("@type", MySqlDbType.VarChar);
            commAdd.Parameters["@type"].Value = type;
            commAdd.Parameters.Add("@idFact", MySqlDbType.Int32);
            commAdd.Parameters["@idFact"].Value = idFact;

            /* execute query */
            int rowsAffected = commAdd.ExecuteNonQuery();

            /* write results */
            answer = rowsAffected.ToString();
        }

        /* return answer */
        return "Rows affected:" + answer;

    }

    public string updateCar(int id, string name, string type, string factory)
    {

        /* row for query (find id of factory) */
        string queryFind = "select id from factory where name = @name";
        /* id of factory */
        int idFact = 0;

        try
        {
            /* make connection and query */
            MySqlConnection connection = new MySqlConnection(connectionRow);
            connection.Open();
            MySqlCommand command = new MySqlCommand(queryFind, connection);

            /* add all paramaters for request */
            command.Parameters.AddWithValue("@name", factory);

            /* read database (find id of query) */
            MySqlDataReader reader = command.ExecuteReader();
            reader.Read();
            idFact = Convert.ToInt32(reader[0].ToString());

            /* close connection */
            connection.Close();
        }
        catch (Exception exc)
        {
            /* return error */
            return exc.Message + "\r\n";
        }

        /* row for query */
        string queryAdd = "update car set name = @name, type = @type, idFact = @idFact where id = @id";
        /* answer for query */
        string answer = "";

        using (MySqlConnection connection = new MySqlConnection(connectionRow))
        {
            /* make connection and query */
            connection.Open();
            MySqlCommand commUpdate = new MySqlCommand(queryAdd, connection);

            /* add all paramaters for request */
            commUpdate.Parameters.Add("@id", MySqlDbType.Int32);
            commUpdate.Parameters["@id"].Value = id;
            commUpdate.Parameters.Add("@name", MySqlDbType.VarChar);
            commUpdate.Parameters["@name"].Value = name;
            commUpdate.Parameters.Add("@type", MySqlDbType.VarChar);
            commUpdate.Parameters["@type"].Value = type;
            commUpdate.Parameters.Add("@idFact", MySqlDbType.Int32);
            commUpdate.Parameters["@idFact"].Value = idFact;

            /* execute query */
            int rowsAffected = commUpdate.ExecuteNonQuery();

            /* write results */
            answer = rowsAffected.ToString();
        }

        /* return answer */
        return "Rows affected:" + answer;

    }

    public string getAllFactories()
    {

        /* answer for query */
        string answer = "";
        /* row for query */
        string query = "select * from factory";

        try
        {
            /* make connection and query */
            MySqlConnection connection = new MySqlConnection(connectionRow);
            connection.Open();
            MySqlCommand command = new MySqlCommand(query, connection);

            /* read database */
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                answer += "Id:" + reader[0].ToString() + " Name:" + reader[1].ToString() + " Country:" + reader[2].ToString() + "\r\n";
            }
        }
        catch (Exception exc)
        {
            /* return error */
            return exc.Message + "\r\n";
        }

        /* return answer */
        return answer;

    }

	public CompositeType GetDataUsingDataContract(CompositeType composite)
	{
		if (composite == null)
		{
			throw new ArgumentNullException("composite");
		}
		if (composite.BoolValue)
		{
			composite.StringValue += "Suffix";
		}
		return composite;
	}
}
